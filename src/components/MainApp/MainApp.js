import React, { Component } from 'react';
import { BrowserRouter as Switch, Route } from 'react-router-dom';

import { Grid } from 'semantic-ui-react';
import Market from '../Market/Market';
import Home from '../Home/Home';
import UserHeader from './components/UserHeader'

class MainApp extends Component {
    render() {
        return (
            <span>{/* TODO: change type ? */}
                <Grid.Row>
                    <UserHeader type={this.props.location.pathname.toLowerCase().slice(1)} />
                </Grid.Row>
                <Switch>
                    <Route path='/home' component={(props) => <Home {...props} type="home" />} />
                    <Route path='/buy' component={(props) => <Market {...props} type="buy" />} />
                    <Route path='/sell' component={(props) => <Market {...props} type="sell" />} />
                </Switch>
            </span>
        );
    }
}


export default MainApp;