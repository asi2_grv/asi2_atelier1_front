import React, { Component } from 'react';
import { Segment, Header, Icon } from 'semantic-ui-react';
import UserInfo from '../../UserInfo/UserInfo';

class UserHeader extends Component {
    render() {
        let content;
        if (this.props.type === "buy") {
            content =
                (<Header.Content>
                    BUY
                    <Header.Subheader>Buy the cards you want</Header.Subheader>
                </Header.Content>);
        }
        else if (this.props.type === "sell") {
            content =
                (<Header.Content>
                    SELL
                    <Header.Subheader>Sell your card to get money</Header.Subheader>
                </Header.Content>);
        }
        else if (this.props.type === "home") {
            content =
                (<Header.Content>
                    HOME
                    <Header.Subheader>Navigate through the menus</Header.Subheader>
                </Header.Content>);
        }
        return (
            <Segment clearing>
                <UserInfo />
                <Header as='h3' floated='left'>
                    <Icon name="money" />
                </Header>
                {content}
            </Segment>
        );
    }
}

export default UserHeader;