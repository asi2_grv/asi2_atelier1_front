import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardReducer from './cardReducer';
import viewReducer from './viewReducer';

const globalReducer = combineReducers({
    userReducer: userReducer,
    cardReducer: cardReducer,
    viewReducer: viewReducer,
 });
export default globalReducer;