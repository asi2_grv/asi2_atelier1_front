const cardReducer= (state={current_card:undefined, card_list_to_sell:[]},action) => {
    switch (action.type) {
        case 'SELECT_CARD':
            return {...state, current_card:action.current_card};
        
        case 'CLEAR_CARD':
            return {...state, current_card:undefined};

        case 'CARD_LIST_TO_SELL':
            return {...state, card_list_to_sell:action.card_list_to_sell};

        default:
        return state;
    }
}

export default cardReducer;