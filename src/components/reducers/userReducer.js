const userReducer= (state={user_list:[], current_user:undefined},action) => {
    switch (action.type) {
        case 'SELECT_USER':
            return {...state, current_user:action.current_user};

        case 'LOAD_USERS':
            let new_user_list=[...action.user_list];
            return {...state, user_list:new_user_list};

        default:
        return state;
    }
}

export default userReducer;