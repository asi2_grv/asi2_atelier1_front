import { Component } from 'react';
import { BrowserRouter as Redirect } from 'react-router-dom';
import { Button, Card, Grid, Icon, Image } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { selectView, clearCard } from '../actions';

class Home extends Component {

    constructor(props) {
        super(props);
        this.props.dispatch(clearCard());
        this.routeChange = this.routeChange.bind(this);
    }
    routeChange = (path) => {
        this.props.history.push(path);
    }

    render() {
        if(this.props.current_user === undefined){
            return(<Redirect to="/signin"/>)
        }
        return (
            <Grid>
                <Grid.Row />
                <Grid.Row />
                <Grid.Row>
                    <Grid.Column width={2} />
                    <Grid.Column width={5}>
                        <Card>
                            <Card.Content>
                                <Image alt="sell_img" src="https://cdn2.iconfinder.com/data/icons/finance/512/Loan-256.png" />
                            </Card.Content>
                            <Button attached='bottom' onClick={() => { 
                                this.props.dispatch(selectView('sell'));
                                this.routeChange('/sell'); }}>
                                <Icon name="money" />
                                Sell Cards
                            </Button>
                        </Card>
                    </Grid.Column>


                    <Grid.Column width={2} />

                    <Grid.Column width={5}>
                        <Card>
                            <Card.Content>
                                <Image alt="buy_img" src="https://cdn2.iconfinder.com/data/icons/finance/512/Card_Payment-256.png" />
                            </Card.Content>
                            <Button attached='bottom' onClick={() => { 
                                this.props.dispatch(selectView('buy'));
                                this.routeChange('/buy'); }}>
                                <Icon name="shopping cart" />
                                Buy Cards
                            </Button>
                        </Card>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row />
                <Grid.Row />
                <Grid.Row>
                    <Grid.Column width={16} >
                        <Grid>
                            <Grid.Row columns={3}>
                                <Grid.Column />
                                <Grid.Column textAlign="center">
                                    <Card>
                                        <Card.Content>
                                            <Image alt="play_img" src="https://cdn2.iconfinder.com/data/icons/multimedia-icons-2/512/Gamepad-256.png" />
                                        </Card.Content>
                                        <Button attached='bottom' onClick={() => { 
                                            this.props.dispatch(selectView('play'));
                                            this.routeChange('/play'); }}>
                                            <Icon name="gamepad" />
                                            Play Game
                                        </Button>
                                    </Card>
                                </Grid.Column>
                                <Grid.Column />
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );    
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    } };
  
export default connect(mapStateToProps)(Home);