import React, { Component } from 'react';
import { Header, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';

class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.user = this.props.user;
    }

    render() {
        if(this.props.current_user !== undefined){
            return (
                <Header as='h3' floated='right'>
                    <Icon name="user circle outline" />
                    <Header.Content>
                        <span id="userNameId">{this.props.current_user.login}</span>
                        <Header.Subheader><span>{this.props.current_user.account.toFixed(2)}</span>$</Header.Subheader>
                    </Header.Content>
                </Header>
            );
        } else {
            return(<div></div>);
        }      
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    }
};

export default connect(mapStateToProps)(UserInfo);