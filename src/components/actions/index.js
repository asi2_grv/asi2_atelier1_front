export const selectuser=(current_user)=>{
    return {type:'SELECT_USER',current_user:current_user};
}

export const loadUsers=(user_list)=>{
    return {type:'SELECT_USER',user_list:user_list};
}

export const selectCard=(current_card)=>{
    return {type:'SELECT_CARD',current_card:current_card};
}

export const selectView=(current_view)=>{
    return {type:'SELECT_VIEW',current_view:current_view};
}

export const loadCardListToSell=(card_list_to_sell)=>{
    return {type:'CARD_LIST_TO_SELL',card_list_to_sell:card_list_to_sell};
}

export const clearCard=()=>{
    return {type:'CLEAR_CARD',current_card:undefined};
}