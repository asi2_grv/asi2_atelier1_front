export const createUser=(process_data, user)=>{
    fetch('http://localhost:8082/user', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    }).then((response) => { 
        process_data(user) });
}

export const updateUser=(process_data, user_id)=>{
    fetch(`http://localhost:8082/user/` + user_id, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then((response) => { return response.json() })
        .then((data) => {
            process_data(data);
    });
}

export const connectUser=(user)=>{
    return fetch(`http://localhost:8082/auth?login=${user.login}&pwd=${user.pwd}`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then((response) => { return response.json() });
}

export const updateCardToSell=(process_data)=>{
    fetch(`http://localhost:8082/cards_to_sell`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then((response) => {return response.json()})
    .then((data) => {
        process_data(data);
    });
}

export const buy_card=(process_data, body)=>{
    fetch('http://localhost:8082/buy', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => { 
        process_data() });
}

export const sell_card=(process_data, body)=>{
    fetch('http://localhost:8082/sell', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => { 
        process_data() });
}